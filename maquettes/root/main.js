$( document ).ready(function() {

    var aside = $('#more_about_ethernium_container');
    var open = false;

    $('#more_ethernium').click( function(e) {
        if(open) {
            open = false;
            aside.hide();
        }
        else {
            open = true;
            aside.show();
        }
    });

});
