(function(){
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {
		var data = google.visualization.arrayToDataTable([
			['Date', 'Number of users'],
			['05/05',  1000],
			['06/05',  1170],
			['07/05',  660],
			['08/05',  1030]
		]);

		var options = {
			title: 'User activity',
			curveType: 'function',
			legend: { position: 'bottom' },
			backgroundColor : "#d5d5d5"
		};

		var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

		chart.draw(data, options);
	}
})();